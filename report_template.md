---
author: "*Luke Bisee, Harly Werkin, Notta Klu, Ben Gonna, Astu Dent*"
title: Paper Title
subtitle: "*Z_group 999*"
institute: #module -- #module_title, #institute
date: #date

abstract: |
   **Abstract:** _Write an abstract that briefly summarizes the topic, context, research question, results, and conclusions._\
    **Remove the text in _italics_, and this line, before submitting.**"
...



# Introduction

_The introduction gives the
reader an overview of your paper comprising the topic of the question,
the context in which the topic and question should be interpreted, the
actual question, the data set used to answer the question, how the
question is to be answered, a brief summary of the results, and the
conclusions._

_Context: the context describes the environment in which the topic is
situated and the question should be interpreted._

_Topic: what is the problem or topic you are investigating?  Why is it interesting?_

_Research question and hypotheses: this is a crisp statement of your
research question, and  your null and alternative hypotheses.  These should match #filename(research_question.yml)._

_Dataset: identify your dataset, including not only the _Data.world_ URL, but
also the original source of the data.  Describe the columns in your
dataset that correspond to your independent and dependent variables:
in what units are the values expressed?  What do the values _mean_?
Are they interval, ordinal, or nominal?_

_Results: Briefly state the answer to your research question._

_Implications/conclusions: What do your results mean?  Foreshadow the Discussion and Conclusion
sections here, with a sentence each._


_Guide to the rest of the paper: at the end of the introduction, it's
customary to describe what the remaining sections contain._


# Visualization

_This section should describe the dataset in detail, then present 
a plots showing a visualization of the data that would suggest an
answer for the research question._

_The visualization should be:_

* _A *scatterplot* if the question is about correlation._
* _A set of *boxplots* if the question is about comparison of means._
* _A set of *stacked barcharts* if the question is about comparison of
  proportions._
  

_If the question is about correlation or comparison of
means, there should be a *histogram* showing the frequencies of the
dependent variable values.  The text should interpret the meaning of
this histogram as your judgement whether the data are normally
distributed or not._

_The plots should be taken from the output of `visualization.R` and
the text should interpret them according to your judgement as to what they suggests about the answer to your research question._


# Analysis

_This section should describe:_

1. _The test statistic used to answer the question, and why it is
appropriate for the data.  In particular, you should say how your
chosen test statistic is suitable for your data's apparent distribution._
2. _The value of the test statistic._
3. _The *p-value* reported by R, and whether this value suggests the null hypothesis can be rejected in favor of the alternative hypothesis._



# Conclusions

_What do the results of your analysis mean?_

_If you reject the null hypothesis, what does this tell you?  Is the
relationship causal or coincidental?_  

_If you do not reject the null hypothesis, does this mean the answer
to your question is "no," or does it suggest that you don't have
enough data?_




# References

_All facts, claims, and assertions must be supported by a citation and
reference to the literature._

_References should be in Harvard format, although we are more interested
in *correct* references as long as you use a *consistent* format._


