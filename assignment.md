<!-- If you can see this line, you are reading the wrong file.  Please
     see instructions.pdf, instructions.html, or instructions.txt
     instead. -->

The Final Report deliverable of the coursework is due before 23:59 Friday, #cw_report_due.

Please clone
https://bitbucket.org/jnoll2/final-report-assignment.git for
detailed instructions on how to create and submit your final report.  This repository also has a template for the
`final_report.docx` file that you need to commit to *your* repository.


Also note that for this deliverable, _you must also submit a PDF
version of your final report via this "Final Report" assignment page on Canvas._
Email submissions are NOT allowed and will be
silently ignored.

