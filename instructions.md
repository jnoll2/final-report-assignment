---
author: #module_leader
date: #date
documentclass: hitec
fontfamily: opensans
fontsize: 12
...
# Overview


The final report brings together all of the previous deliverables into
a coherent report documenting the answer to your research question.

**NOTE:**  The final report is an *extra credit* deliverable available
  *only* to those groups who passed the previous three deliverables
  (research question, visualization, and analysis):
  
1. If your group scored less than 50 on *any* of the research question,
visualization, and analysis deliverables, you may update them for
additional credit (capped at 50), but you may *not* submit a final report.

    Your final coursework score will be the mean of your research
question, visualization, and analysis deliverables, or 65, whichever is lower.

    Do not submit a final report, as it will not count toward
your coursework score.

2. If your group scored 50 or above on *all* of the research question,
visualization, and analysis deliverables, you may opt to skip the
final report.  

    If you do not submit a final report, your coursework
score will be the mean of your research question, visualization, and
analysis deliverables, or 65, whichever is lower.

3. If your group scored 50 or above on *all* of the research question,
visualization, and analysis deliverables, you may opt to submit a
final report.  

    If you do submit a final report, your coursework
score will be the mean of your research question, visualization, 
analysis, and final report deliverables, with no cap.

# Instructions

## Check your scores

The final report is available to those groups who scored 50 or above
on *all* three of the research question, visualization, and analysis deliverables.

To check your scores:

1. Visit our Canvas site.
2. Follow the "Grades" link from the left menu.
3. Select "7COM1079" from the dropdown.
4. Check the following _three_ scores:

    1. Research Question.
    2. Visualization.
    3. Analysis.

5. If you scored less than 50 on any of these, you should update those
deliverables that have a score less than 50, for re-evaluation.  You
are *not* eligible to submit a final report, but you can still pass
this module if you correct your deliverables successfully.



## Update your previous deliverables

If you scored less than 50 on the research question,  visualization,
or analysis deliverable, you *may* revise the deliverable in question
and re-submit for re-evaluation.  Your score for each deliverable will
be capped at 50, and your coursework score will be capped at 55.

If you revised your dataset, research question, visualization, or
analysis since the respective deliverable deadline, you *must* update
your deliverables so they are consistent.

In either case, do the following:

#. Clone your group's BitBucket repository.

#. Update #filename(research_question.yml) to reflect your _current_
 research question and dataset, and fix any syntax errors.

#. Update #filename(visualization.R) to reflect your _current_
 research question and dataset, and fix any syntax errors.

#. Update #filename(analysis.R) to reflect your _current_ research
 question and dataset, and fix any syntax errors.



## Prepare your report

This option is available *only* to groups that scored 50 or above on
*all* of the research question, visualization, and analysis
deliverables.

Groups that meet these criteria are listed in the file #filename(eligible.txt).

The final report is, however,  optional: 
if you choose not to submit a final report, your coursework score will be the
the mean of the research question, visualization, and analysis
deliverables, capped at 65.

If you are eligible to submit a final report, and you choose to do so,
here are the tasks:

#. Check #filename(eligible.txt) to be sure your group is eligible to submit a final report.

#. Copy #filename(final_report.docx) from _this_ repository to your group workspace.

#. Edit the report heading in _final_report.docx_:

    1. Replace "Paper Title" with the title of your report.  It should
    reflect the topic and research question.
    2. Replace "Z_group 999" with your group number.
    3. Put the name of *each* group member, as it appears on Canvas,
    above the date.
    4. Change the date to the final report deadline.

#. Write your report, following the suggestions in the report template.

    Your report should include the following sections:
    
    * Abstract, summarizing the report.
    * Introduction, that states the context, topic, and research question, describes the dataset, and foreshadows the results and conclusions.
    * Visualization, that describes the data.
    * Analysis, that reports the results of your statistical test.
    * Conclusion, that discusses the implications of your results,
      including whether they are statistically significant.
    * References, for citations of *all* facts, claims, and assertions
      in the body of the report, using Harvard (author-date) style, or
      IEEE (numbered) style.
    
#. Proofread, then commit your revised #filename(final_report.docx) file.

#. Submit your report to _grammarly.com_ to help identify grammar,
 punctuation, and spelling mistakes.
 
#. Revise your final report taking into account suggestions from
 _grammarly.com_.
 
#. Proofread, then commit your revised #filename(final_report.docx) file.
 

#. Push your workspace to BitBucket by the deadline specified in the "Final Report" section of the coursework specification (#cw_report_due).

#. Create a *PDF* version of your report and submit it via the "Final  Report Submission" page on Canvas by the deadline specified in the
   "Final Report" section of the coursework specification  (#cw_report_due).

# Notes

* The final report should be four (4) pages, not including references.
* _All_ facts must have a citation and reference to the literature.
* _All_ assertions must have a supporting citation and reference to the literature.
* _All_ claims must be supported by a citation and reference to the literature.
* Text that you cut and paste from the web or other source _must_ be enclosed in quotation marks, and include a citation and reference to the source of the text.
* Text that you _paraphrase_ (including text that you submit to a synonym substituter, or subject to "double translation"), must have a citation and reference to the source of the text.
* Text that is copied without attribution (citation and reference to the literature) will be marked with a score of zero (0) per the coursework rubric.
* See the "Referencing" topic on the library's  "SkillUP" canvas site (`https://herts.instructure.com/courses/61421`) for details about how to cite various sources.
