---
author: "*Luke Bisee, Harly Werkin, Notta Klu, Ben Gonna, Astu Dent*"
title: An Analysis of Variation Among Grader's Scores
subtitle: "*Z_group 999*"
institute: #module -- #module_title, #institute
date: #date

abstract: |
   **Abstract:** \
   *Context:* Large classes combined with the need for  authentic
   assessment and rapid feedback, require multiple graders to
   concurrently grade assignments.  This raises the question of
   consistency: do different graders mark assignments differently?\
   *Question:* This study asks, is there a difference in the median
   score assigned by different markers on the same assignment?\
   *Method:* We analyzed the marks assigned by ten graders for the first
   assignment of the autumn 2021 instance of the Team Research and
   Development module at the University of Hertfordshire.\
   *Results:* Although there appears to be a wide variation in the
   median scores among graders, a pairwise comparison of scores assigned by each
   marker revealed only one significant difference between a pair of
   graders.\
   *Conclusion:* The results suggest that the grading rubric used by
   graders produces acceptably consistent results, but there is some room
   for improvement.
...



# Introduction

To meet the need for authentic assessment [@Svinicki_2004_Authentic]
and meet increasing industry need for soft skills [@Blyth_2019_Soft],
engineering and computer science classes often include project-based
assignments that require human judgment to grade.  

However, such authentic assessments raise issues of reliability of
marking among multiple markers [@Montgomery_2002_Authentic].

Rubrics--"descriptive rating scales that are particularly useful for
scoring when judgment about the quality of an answer is required"
[@Brookhart_1999_Art]-- are frequently used to specify grading criteria
and provide a framework to assist graders in assessing work
consistently [@Moskal_2000_Scoring].  But do rubrics really produce
consistent results across different graders?

This study aims to compare marks assigned by multiple graders on the
same assignment.  Specifically, we ask the following research question:

_Is there a difference in the median scores assigned for the same
assignment among different markers?_

The null hypothesis is:

$H_0$: there is no difference in the median scores assigned for the same
assignment among different markers.

The alternative hypothesis is:

$H_{alt}$: there is a difference in the median scores assigned for the same
assignment among different markers.

To answer this question, we used a dataset comprising scores from the
"Research Question" assignment of the autumn 2021 edition of the "Team
Research and Development" module that is part of the Master's in
Computer Science program at the University of Hertfordshire.  We
compared scores (the _score_ column in the dataset) assigned by ten
graders (the _grader_ column) who marked  assignments
submitted by project groups of five students.  Graders submitted marks
as answers to a 30-question "Yes/No" rubric structured as a decision
tree.  The rubric was created in attempt to achieve greater
consistency among markers, and enable rapid grading of assignments
[@Noll_2021_Designing].

We performed a pairwise comparison of the scores assigned by the ten
markers, and found only one instance where a pair of markers had a
significant difference in scores.  This suggests the "Yes/No" rubric
approach produces adequate consistency, but with some room for
improvement.

The rest of this paper is structured as follows:  in the next section,
we present a visual view of the data, followed by a statistical
analysis.  We conclude with a discussion of the implications of the results.

![Figure 1: Research question assessment rubric.](rubric.png){width=90%}

# Visualization

The dataset used for this study comprises 227 scores derived from the
rubric responses of ten different graders.  The markers are
identified as "A" through "J" to preserve anonymity.  Scores were
computed from the answers to 30 questions on a "Yes/No" rubric
completed by each grader (see Figure 1); the answers were converted to a numeric
score (out of 100) using a rule-based system [@Noll_2021_Designing].

![Figure 2: Scores assigned by each grader.  Graders are identified as 'A' through 'J' to preserve anonymity.](boxplot.pdf){width=90%}

Figure 2 shows the variation in grades assigned by all ten graders.
This figure suggests that there is wide variation in the scores
assigned by different graders: the median scores range from barely 50
for grader A, to over 80 for grader E.  Furthermore, graders D, E, H,
and I appear to have scores that are skewed toward the high end,
although grader D also produced the lowest overall score, and grader E
produced two low outlier scores.

![Figure 3: Distribution of scores assigned by all markers.](histogram.pdf){width=90%}

Figure 3 depicts a histogram showing the frequency distribution of
grades assigned by all markers.  This histogram shows that the scores
are skewed toward the high end, rather than following the usual "bell
curve" of a normal distribution as would be expected [@Kulick_2008_Impact]

# Analysis

Grader:&nbsp;&nbsp;   A    B    C    D    E    F    G    H    I    J
-------------------  --   --   --   --   --   --   --   --   --   --
\#                   17   25   17   25   17   25   25   42   17   17
1st quartile         40   48   49   62   71   52   51   65   59   54
Median               51   57   71   73   82   67   65   76   74   71
3rd quartile         82   73   77   78   86   82   82   86   77   82

*Table 1: Range of scores assigned by each grader.*

Figure 3 suggests the scores are not normally distributed, so we used
the non-parametric _pairwise.wilcoxon.test_ function provided by the R
statistical programming language [@R_2021_R].  This test performs a
Wilcoxon-Mann-Whitney test [@Mann_1947_Test] comparing the set of
scores for each grader to that of each other grader, in a pairwise
fashion.  The Wilcoxon-Mann-Whitney test assesses whether two sets of
values are distributed differently; in other words, it tests whether,
when all values from both sets are ordered, one set is significantly
skewed toward one end of the scale [@Dalgaard_2008_Introductory].


  Grader     A       B        C       D       E       F       G       H      I
 -------- ------- --------- ------- ------- ------- ------- ------- ------- ---------
  B         1.0      \-       \-      \-      \-      \-      \-      \-      \-
  C         1.0     1.0       \-      \-      \-      \-      \-      \-      \-
  D         1.0     0.24     1.0      \-      \-      \-      \-      \-      \-
  E         0.42    0.15     0.92    1.0      \-      \-      \-      \-      \-
  F         1.0     1.0      1.0     1.0     1.0      \-      \-      \-      \-
  G         1.0     1.0      1.0     1.0     1.0     1.0      \-      \-      \-
  H         0.27    0.02     0.90    1.0     1.0     1.0     1.0      \-      \-
  I         1.0     0.61     1.0     1.0     1.0     1.0     1.0     1.0      \-
  J         1.0     1.0      1.0     1.0     1.0     1.0     1.0     1.0     1.0  

*Table 2: _P_-values resulting from pairwise Wilcoxon test comparing pairs of graders' scores.  Only (B, H) is significant at $\alpha < 0.05$.*

Table 1 shows the _P_-values resulting from this pairwise analysis.
Each entry shows the probability that the observed difference between
the two sets of grades is due to random variation, rather than an
actual difference in the distribution of values
[@Biau_2010_P].  Table 1 shows that there is a potentially
significant difference (at $\alpha < 0.05$) in only one case,
that of grader B vs. grader H (_P_-value = 0.02).  As such, we can
consider rejecting the null hypothesis only in the case of grader B vs. grader H;
in all other comparisons, there is a high probability that the
observed differences are due to random variation.

# Conclusions

At first glance, Figure 2 seems to show a wide variation among
graders.  However, with one exception, the Wilcoxon-Mann-Whitney test
does not reveal a significant difference in scores.

The literature related to consistency of marking using rubrics
presents a mixed picture.  For example, in their review rubric use in
higher education, Reddy and Andrade  cite four
studies of rubric reliability, two of which reported good reliability,
a third reported poor initial reliability that improved to excellent
after graders were trained, and one reported moderate reliability
[@Reddy_2010_Review]. 

Studies of reliability tend to use reliability metrics such as Cohen's
kappa [@Landis_1977_Application], so it is difficult to compare their
results to ours.  Stellmack & colleagues used Cohen's kappa to measure
reliability of grading for a writing assignment, and found
excellent^["Excellent" meaning a kappa value greater than .9, where agreement is defined as being within 1 point.]
agreement among multiple graders [@Stellmack_2009_Assessment].  Noll &
colleagues also reported excellent agreement as measured by weighted
kappa [@Cohen_1968_Weighted] for the rubric used in this study
[@Noll_2021_Designing].

These studies suggest that the variability we observed is to be
expected, and the lack of significant differences in scores produced
using the dichotomous "Yes/No" rubric is a sign of adequate
reliability among graders, which is consistent with guidelines [@Stegeman_2016_Designing].

## Limitations and Potential Future Work

There are, however, some limitations with the current study.  First,
the number of projects graded by each marker ranges from 17 to 25
(see Table 1),
except for grader H who marked 42 projects.  The fact that grader H is
part of the only pair with a significant difference in mean score
raises the possibility that, with more data points, more significant
differences might be revealed.

Second, the graders each graded different projects.  Projects appear
to have been allocated randomly, but there is a possibility that the
observed differences in median scores is due to variability among
_projects_ rather than _graders_.

Finally, median score is not the only
measure of consistency among graders.  As mentioned previously,
measures of inter-rater reliability such as Cohen's kappa provide a
more detailed assessment of consistency, by examining differences
among scores assigned by graders to the _same_ item.  


Our results suggest that consistency among markers using the "Yes/No"
rubric is acceptable, but in light of these limitations, future work
could involve having different graders mark the same sample, and
compare results using percentage agreement or Cohen's kappa.

# References


