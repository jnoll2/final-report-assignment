## jhnoll@gmail.com
ROOT:=$(shell $(HOME)/bin/findup .sgrc)
SOURCE_DIR=.
include $(ROOT)/tools/Makefile.in
BIB=example.bib

# This is used by tools/Makefile.in to create 'all:' target.
INSTALL_OBJECTS=README.txt instructions.txt instructions.pdf instructions.html rubric.xlsx final_report.docx example.pdf report_template.tex

all: $(INSTALL_OBJECTS)

README.txt: ${TOOLS.dir}/templates/lab-README.md
	$(PP) -Dcomponent=${*} $(META_DATA) $< | $(PANDOC) --standalone -t plain -o $@

README.md: ${TOOLS.dir}/templates/lab-README.md
	$(PP) $< > $@

rubric.xlsx: $(ROOT)/grades/final-report/rubric.xlsx
	cp $< $@

final_report.docx: report_template.md $(BIB) reference.docx 
	$(PP) $(CONFIG_DATA) $< | $(PANDOC) --standalone -t docx --reference-doc=$(lastword $^) --bibliography=$(BIB) -o $@
report_template.tex: report_template.md $(BIB) 
	$(PP) $(CONFIG_DATA) $< | $(PANDOC) --standalone -t latex --bibliography=$(BIB) -o $@

%.docx: %.md $(BIB) reference.docx 
	$(PP) $(CONFIG_DATA) $< | $(PANDOC) --standalone -t docx --reference-doc=$(lastword $^) --citeproc --bibliography=$(BIB) -o $@

assignment.html: assignment.md
	$(PP) $(CONFIG_DATA) $< | $(PANDOC) -t html --reference-doc=$(lastword $^) --bibliography=$(BIB) -o $@

example.pdf: example.docx
	/usr/lib/libreoffice/program/soffice --convert-to pdf $< --headless

%.pdf: %.dot
	dot -Tpdf $< > $@



include $(ROOT)/tools/Make.rules
clean:
	rm -f $(INSTALL_OBJECTS)


